// modules/Repo.js
import React from 'react'

export default React.createClass({
  render() {
    return (
      <div>
        <h2>Repo: {this.props.params.repoName}</h2>
        <p>Submitted by user: {this.props.params.userName}</p>
      </div>
    )
  }
})