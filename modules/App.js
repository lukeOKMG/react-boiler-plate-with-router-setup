// modules/App.js
import React from 'react'
import { Link } from 'react-router'
import { IndexLink } from 'react-router'

import NavLink from './NavLink'

export default React.createClass({
    render() {

    return (
      <div>
        <h1>Lukes Dope Home Page</h1>
        <ul role="nav">
			<li><IndexLink to="/" activeClassName="active">Home</IndexLink></li>
			<li><NavLink to="/about">About</NavLink></li>
			<li><NavLink to="/repos">Repos</NavLink></li>
        </ul>

        {/* add this, allows for nested navigation */}
        {this.props.children}
      </div>
    )
  }

})