# Setting up the Project

First you'll need [Node.js](https://nodejs.org) and the package manager
that comes with it: [npm](https://www.npmjs.com/).

Once you've got that working, head to the command line where we'll set
up our project.

## Clone the Boiler Plate

```
git clone https://lukeOKMG@bitbucket.org/lukeOKMG/react-boiler-plate-with-router-setup.git
cd react-boiler-plate-with-router-setup
npm install
npm start (dev mode)
NODE_ENV=production npm start (production mode)
```

Now open up [http://localhost:8080](http://localhost:8080)

Feel free to poke around the code to see how we're using webpack and npm
scripts to run the app.

You should see a "React / Router Boiler Plate" message in the browser.

## Make Some Changes

Add a component and Link that componant to the nav to create another page. 

Happy coding! 

---
